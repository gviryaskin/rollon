package com.gviryaskin.rollon.app;


import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import com.gviryaskin.rollon.app.fragment.*;
import com.gviryaskin.rollon.app.fragment.NavigationDrawerFragment;
import com.gviryaskin.rollon.app.util.BaseActivity;

public class RoutesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);
      NavigationDrawerFragment fragment= ((NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.routes_navigation_drawer));
        fragment.setUp(
                R.id.routes_navigation_drawer,
                (DrawerLayout) findViewById(R.id.routes_drawer_layout));

    }


}
