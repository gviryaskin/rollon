package com.gviryaskin.rollon.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;

/**
 * Created by gviryaskin on 07.05.14.
 */
public class AppDialogFactory {
    private AppDialogFactory(){}
    public enum DialogId{WhatsNew,Support,About,DeleteRoute,Exit,SaveError,RemoveError,Share}

    public static AppDialog getDialog(DialogId id,AppDialog.Callback callback){
        switch (id){

            case WhatsNew:
                break;
            case Support:
                break;
            case About:
                break;
            case DeleteRoute:
                return new DeleteRouteAppDialog(callback);
            case Exit:
                return new ExitAppDialog(callback);
            case SaveError:
                break;
            case RemoveError:
                break;
            case Share:
                break;
        }
        return null;
    }

    /////////////////////////////////////////////
    //AppDialog
    public static abstract class AppDialog extends DialogFragment implements DialogInterface.OnClickListener{
        //abstract
        public static interface Callback{
            void onClick(int which);
            void onDismiss();
            void onCancel();
            //TODO methods
        }
        enum ButtonsAmount{One,Two,Three}
        abstract int getTitleId();
        abstract boolean isDialogCancelable();
        abstract int getMessageId();
        abstract ButtonsAmount getButtonsAmount();
        abstract ArrayList<Integer> getButtonsNameId();

        //Stuff
        private  Callback mCallback;

        public AppDialog(Callback callback){
            if(callback==null)
                throw new IllegalArgumentException("callback is null!");
            mCallback=callback;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getTitleId());
            builder.setCancelable(isDialogCancelable());
            builder.setMessage(getMessageId());
            //Buttons
            ArrayList<Integer>buttons = getButtonsNameId();
            builder.setPositiveButton(getString(buttons.get(0)),this);
            if(getButtonsAmount()!=ButtonsAmount.One){
                builder.setNegativeButton(getString(buttons.get(1)),this);
                if(getButtonsAmount()!=ButtonsAmount.Two)
                    builder.setNeutralButton(getString(buttons.get(2)),this);
            }
            return builder.create();
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch(which){
                case Dialog.BUTTON_POSITIVE:
                    mCallback.onClick(0);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    mCallback.onClick(1);
                    break;
                case Dialog.BUTTON_NEUTRAL:
                    mCallback.onClick(2);
                    break;
            }
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            mCallback.onDismiss();
            super.onDismiss(dialog);
        }


        @Override
        public void onCancel(DialogInterface dialog) {
            mCallback.onCancel();
            super.onCancel(dialog);
        }
    }
    /////////////////////////////////////////////
    //TODO make custom AppDialog

}
