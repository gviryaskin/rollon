package com.gviryaskin.rollon.app.userdata;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by gviryaskin on 05.05.14.
 */

public class UserDataManagement {
    private UserDataManagement(){}
    private static Logger mLogger = LoggerFactory.getLogger(UserDataManagement.class);
    /////////////////////////////////////////////
    //Routes
    private final static String ROUTE_FOLDER="route";
    private final static String ROUTE_LIST="list";

    private static HashMap<Long,UserRoute> mRoutes=new HashMap<Long, UserRoute>();
    private static ArrayList<UserRoute> mRoutesList=new ArrayList<UserRoute>();

    private static synchronized void loadRouteList(Context context){
        try{
            mRoutes.clear();
            mRoutesList.clear();

            //FileInputStream fis = context.openFileInput(String.format("%s/%s", ROUTE_FOLDER, ROUTE_LIST));
            File mydir = context.getDir(ROUTE_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
            File fileWithinMyDir = new File(mydir,ROUTE_LIST); //Getting a file within the dir.
            FileInputStream fis = new FileInputStream(fileWithinMyDir); //Use the stream as usual to write into the file.\

            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            String json = new String(buffer, "UTF-8");
            JSONArray jList = new JSONArray(json);
            mLogger.debug("Loading data");
            for(int i=0;i<jList.length();i++){
                UserRoute route = new UserRoute();
                if(route.setJSONData(jList.getJSONObject(i)))
                    mRoutes.put(route.getId(), route);
            }
        }catch(IOException eIO){
            mLogger.warn("No route list file found");
        }catch (JSONException eJSON){
            mLogger.warn("Route json list file malformed");
        }finally {
            if(mRoutes!=null) {
                mRoutesList.addAll((mRoutes.values()));
            }
        }
    }

    private static synchronized boolean saveRouteList(Context context){
        mLogger.debug("Saving list");
        JSONArray jList =new JSONArray();
        for(UserRoute route:mRoutes.values()) {
            JSONObject jRoute=route.getJSONData();
            if(jRoute!=null)
                jList.put(route.getJSONData());
        }
        try{
            //FileOutputStream fos = context.openFileOutput(String.format("%s/%s",ROUTE_FOLDER,ROUTE_LIST),Context.MODE_PRIVATE);
            File mydir = context.getDir(ROUTE_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
            File fileWithinMyDir = new File(mydir,ROUTE_LIST); //Getting a file within the dir.
            FileOutputStream fos = new FileOutputStream(fileWithinMyDir);
            fos.write(jList.toString().getBytes());
            fos.close();
            return true;

        }catch(FileNotFoundException e){
            mLogger.error(e.getLocalizedMessage());
            return false;
        }catch(IOException e){
            mLogger.error((e.getLocalizedMessage()));
            return false;
        }

    }

    public static ArrayList<UserRoute> getUserRoutes(Context context){
        if(mRoutes.isEmpty())
            loadRouteList(context);
        return  mRoutesList;
    }

    public static UserRoute getRoute(long id){  return mRoutes.get(id); }

    public static boolean loadRoute(Context context, UserRoute route){
        mLogger.debug("Loading route with id:"+route.getId());
        try {
            FileInputStream fis = context.openFileInput(String.format("%s/%d", ROUTE_FOLDER, route.getId()));

            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            String json = new String(buffer, "UTF-8");
            if(!route.setJSONDetail(new JSONObject(json)))
                return false;
            return true;
        }catch(FileNotFoundException eFile){
            mLogger.error(eFile.getLocalizedMessage());
            return false;
        }catch(IOException eIO){
            mLogger.error(eIO.getLocalizedMessage());
            return false;
        } catch (JSONException eJSON) {
            mLogger.error((eJSON.getLocalizedMessage()));
            return false;
        }

    }
    public static boolean saveRoute(Context context,UserRoute route){
        mLogger.debug("Saving changes");
        mRoutes.put(route.getId(), route);
        if(!saveRouteList(context)) {
            mRoutes.remove(route.getId());
            return false;
        }
        try{
            JSONObject jDetails = route.getJSONDetail();
            if(jDetails==null) {
                mRoutes.remove(route.getId());
                return false;
            }
            //FileOutputStream fos =context.openFileOutput(String.format("%s/%d",ROUTE_FOLDER,route.getId()), Context.MODE_PRIVATE);
            File mydir = context.getDir(ROUTE_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
            File fileWithinMyDir = new File(mydir,String.valueOf(route.getId())); //Getting a file within the dir.
            FileOutputStream fos = new FileOutputStream(fileWithinMyDir);
            fos.write(jDetails.toString().getBytes());
            fos.close();
        }catch(FileNotFoundException e){
            mLogger.error(e.getLocalizedMessage());
            mRoutes.remove(route.getId());
            return false;
        }catch(IOException e){
            mLogger.error((e.getLocalizedMessage()));
            mRoutes.remove(route.getId());
            return false;
        }
        mRoutesList.clear();
        mRoutesList.addAll(mRoutes.values());
        return true;
    }

    public static boolean removeRoute(Context context,UserRoute route){
        mLogger.debug("Remove route");
        HashMap<Long,UserRoute> list= new HashMap<Long, UserRoute>();
        list.putAll(mRoutes);
        mRoutes.remove(route.getId());
        mRoutesList.clear();
        mRoutesList.addAll(mRoutes.values());
        if(!saveRouteList(context)) {
            mRoutes = list;
            return false;
        }
        File mydir = context.getDir(ROUTE_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        File fileWithinMyDir = new File(mydir,String.valueOf(route.getId())); //Getting a file within the dir.
        if(!fileWithinMyDir.delete())
            return false;
        //context.deleteFile((String.format("%s/%d",ROUTE_FOLDER,route.getId())));
        mRoutesList.clear();
        mRoutesList.addAll(mRoutes.values());
        return true;

    }
}
