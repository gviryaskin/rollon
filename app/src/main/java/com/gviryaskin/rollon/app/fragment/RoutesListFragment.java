package com.gviryaskin.rollon.app.fragment;

import android.support.v4.app.Fragment;

import com.gviryaskin.rollon.app.model.Route;

import org.jetbrains.annotations.NotNull;

/**
 * Created by gviryaskin on 23.09.14.
 */
public class RoutesListFragment extends Fragment {
    public enum Type {Linear,Grid}
    private Type mCurrentType=Type.Linear;
    private IRouteListener mRouteListener;
    public void setCurrentType(@NotNull Type aListType){
        mCurrentType=aListType;

    }


    public void setRouteListener(@NotNull IRouteListener aRouteListener){mRouteListener=aRouteListener;}

    public interface IRouteListener{
        void onRouteSelected(@NotNull Route aRoute);
    }

}
