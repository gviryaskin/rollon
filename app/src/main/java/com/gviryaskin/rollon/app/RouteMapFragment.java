package com.gviryaskin.rollon.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.gviryaskin.rollon.app.dialog.AppDialogFactory;
import com.gviryaskin.rollon.app.userdata.UserDataManagement;
import com.gviryaskin.rollon.app.userdata.UserRoute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by gviryaskin on 06.05.14.
 */
//TODO Implement make editor share and fragment with map
//TODO two layouts one for editor
//TODO Check list and map after sleep
//TODO points AMount only points with description!
//TODO Mark if need to update list
public class RouteMapFragment extends Fragment {
    private static Logger mLogger = LoggerFactory.getLogger(RouteMapFragment.class);

    private View mView;
    private UserRoute mRoute;
    private boolean mEdit=false;

    private boolean mUpdateData=false;
    public boolean needUpdateData(){return  mUpdateData;}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mUpdateData=false;
        mView = inflater.inflate(R.layout.fragment_route_map, container, false);
        if(mRoute==null)
            throw new IllegalStateException("Route is null!");
        mLogger.debug("Initiating route with id: "+mRoute.getId());
        if(mEdit)
            initialEditor();
        else
            initialMap();
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //Initial UI
    private static ArrayList<Bitmap> mIcons =new ArrayList<Bitmap>();
    private void initialEditor(){
        if(mRoute==null)
            throw new IllegalStateException("Route is null!");
        mLogger.debug("Initial the route editor");
        mView.findViewById(R.id.routeLayout).setVisibility(View.GONE);
        mView.findViewById(R.id.routeEditorLayout).setVisibility(View.VISIBLE);
        if(mRoute.getName()==null)
            mRoute.setName(getString(R.string.new_route_name));
        ((EditText) mView.findViewById(R.id.routeEditorName)).setText(mRoute.getName());
        ((ImageView) mView.findViewById(R.id.routeEditorIcon)).setImageResource(mRoute.getIcon());
        ((TextView) mView.findViewById(R.id.routeEditorLengthValue)).setText((String.valueOf(mRoute.getLength())));
        ((TextView) mView.findViewById(R.id.routeEditorPointsValue)).setText((String.valueOf(mRoute.getPointAmount())));
        if(mRoute.getDescription()==null)
            mRoute.setDescription(getString(R.string.new_route_description));
        ((EditText) mView.findViewById(R.id.routeEditorDescription)).setText(mRoute.getDescription());

        View.OnClickListener iconListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //TODO PopupWindow with change data later
            }
        };
        mView.findViewById(R.id.routeEditorIcon).setOnClickListener(iconListener);
        mView.findViewById(R.id.routeEditorArrow).setOnClickListener(iconListener);
        
        //TODO init map fragment
        mView.findViewById(R.id.routeEditorConfirmButton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mRoute.setName(((EditText) mView.findViewById(R.id.routeEditorName)).getText().toString());
                mRoute.setDescription(((EditText) mView.findViewById(R.id.routeEditorDescription)).getText().toString());

                mLogger.debug(String.format("Saving new route with id %d and name: %s",mRoute.getId(),mRoute.getName()));
                mEdit=false;
                if(UserDataManagement.saveRoute(getActivity(),mRoute)) {
                    mLogger.debug("Saved successful");
                    mUpdateData=true;
                    initialMap();
                }else
                    AppDialogFactory.getDialog(AppDialogFactory.DialogId.SaveError,new AppDialogFactory.AppDialog.Callback() {
                        @Override
                        public void onClick(int which) {
                            Intent intent = new Intent(RouteFragment.ACTION_ROUTE_SWAP)
                                    .putExtra(RouteFragment.TAG_FRAGMENT_TYPE,RouteFragment.FragmentType.List.ordinal());
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                        }

                        @Override
                        public void onDismiss() {
                        }

                        @Override
                        public void onCancel() {
                        }
                    }).show(getChildFragmentManager(),"SAVE_ERROR");
            }
        });
        //mView.invalidate();
    }
    private void initialMap(){
        if(mRoute==null)
            throw new IllegalStateException("Route is null");

        mView.findViewById(R.id.routeLayout).setVisibility(View.VISIBLE);
        mView.findViewById(R.id.routeEditorLayout).setVisibility(View.GONE);

        ((TextView) mView.findViewById(R.id.routeName)).setText(mRoute.getName());
        ((ImageView)mView.findViewById(R.id.routeIcon)).setImageResource(mRoute.getIcon());
        //TODO Init map fragment
        //TODO measure convert
        ((TextView) mView.findViewById(R.id.routeLengthValue)).setText(String.format("%d %s",
                mRoute.getLength(),getString(R.string.km)));
        ((TextView) mView.findViewById(R.id.routePointsValue)).setText(String.valueOf(mRoute.getPointAmount()));
        ((TextView) mView.findViewById(R.id.routeDescription)).setText(mRoute.getDescription());
        //Actions
        mView.findViewById(R.id.routeShare).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                AppDialogFactory.getDialog(AppDialogFactory.DialogId.Share,new AppDialogFactory.AppDialog.Callback() {
                    @Override
                    public void onClick(int which) {
                        //TODO stuff with clicked buttons
                    }

                    @Override
                    public void onDismiss() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        });

        mView.findViewById(R.id.routeEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRoute=new UserRoute(mRoute);
                mEdit=true;
                initialEditor();
            }
        });
        mView.findViewById(R.id.routeDelete).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AppDialogFactory.getDialog(AppDialogFactory.DialogId.DeleteRoute,new AppDialogFactory.AppDialog.Callback() {
                    @Override
                    public void onClick(int which) {
                        mLogger.debug("Removing route");
                        if(which==1)
                            return;
                        if(UserDataManagement.removeRoute(getActivity(),mRoute)){
                            Intent intent = new Intent(RouteFragment.ACTION_ROUTE_SWAP)
                                    .putExtra(RouteFragment.TAG_FRAGMENT_TYPE,RouteFragment.FragmentType.List.ordinal())
                                    .putExtra(RouteFragment.TAG_FRAGMENT_UPDATE,true);
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                           getActivity().onBackPressed();
                        }else
                            AppDialogFactory.getDialog(AppDialogFactory.DialogId.RemoveError,new AppDialogFactory.AppDialog.Callback() {
                                @Override
                                public void onClick(int which) {
                                    Intent intent = new Intent(RouteFragment.ACTION_ROUTE_SWAP)
                                            .putExtra(RouteFragment.TAG_FRAGMENT_TYPE,RouteFragment.FragmentType.List.ordinal());
                                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                                }

                                @Override
                                public void onDismiss() {
                                }

                                @Override
                                public void onCancel() {
                                }
                            }).show(getChildFragmentManager(),"REMOVE_ERROR");
                    }

                    @Override
                    public void onDismiss() {

                    }

                    @Override
                    public void onCancel() {

                    }
                }).show(getChildFragmentManager(),"DELETE_ROUTE");
            }
        });
        mView.findViewById(R.id.routeBack).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        //mView.invalidate();
    }
    public void setRoute(UserRoute route){
        if(route==null)
            throw new IllegalStateException("Route is null");
        mLogger.debug("Starting route with name: "+route.getName());
        mRoute=route;
        mEdit=false;
        if(!mRoute.isLoaded())
            UserDataManagement.loadRoute(getActivity(),mRoute);
    }
    public void newRoute(Context context){
        mLogger.debug("New route editor mode on clear previous data");
        mRoute=new UserRoute();
        //mRoute.setName(context.getString(R.string.new_route_name));
        mEdit=true;
    }
    //adapter
    private static class IconSpinnerAdapter extends BaseAdapter{
        private LayoutInflater mLayoutInflater;
        IconSpinnerAdapter(LayoutInflater inflater){mLayoutInflater=inflater;}
        @Override
        public int getCount() {
            return UserRoute.Icon.values().length;
        }

        @Override
        public Object getItem(int position) {
            return UserRoute.Icon.values()[position];
        }

        @Override
        public long getItemId(int position) {
            return UserRoute.getIconId(((UserRoute.Icon) getItem(position)));
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null)
                convertView=mLayoutInflater.inflate(R.layout.spinner_editor_image_item,parent,false);
            ((ImageView) convertView.findViewById(R.id.routeIcon)).setImageResource(((int)getItemId(position)));
            return convertView;
        }
    }
}
