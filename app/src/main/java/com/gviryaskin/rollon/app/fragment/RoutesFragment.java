package com.gviryaskin.rollon.app.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gviryaskin.rollon.app.R;
import com.gviryaskin.rollon.app.model.Route;

import org.jetbrains.annotations.NotNull;

/**
 * Created by gviryaskin on 22.09.14.
 */
public class RoutesFragment extends Fragment implements RoutesListFragment.IRouteListener {
    private static final String TAG="Routes Fragment";

    private static Boolean mIsTablet,mIsPortrait;
    private RoutesListFragment mRoutesListFragment;
    private RoutesMapFragment mRoutesMapFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Configuration configuration = inflater.getContext().getResources().getConfiguration();
        if(configuration.orientation==Configuration.ORIENTATION_PORTRAIT)
            mIsPortrait=true;
        else mIsPortrait=false;

        if(mIsTablet==null)
            mIsTablet=(configuration.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >=Configuration.SCREENLAYOUT_SIZE_LARGE;

        View view = inflater.inflate(R.layout.fragment_routes,container,false);
        mRoutesListFragment= ((RoutesListFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.routes_list_fragment));
        mRoutesListFragment.setRouteListener(this);
        mRoutesMapFragment= ((RoutesMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.routes_map_fragment));
        if(mIsTablet && mIsPortrait){
            mRoutesListFragment.setCurrentType(RoutesListFragment.Type.Grid);
            view.findViewById(R.id.routes_map_fragment).setVisibility(View.GONE);
        }else if(mIsTablet && !mIsPortrait){
            mRoutesListFragment.setCurrentType(RoutesListFragment.Type.Linear);
        }else if(!mIsTablet && mIsPortrait){
            mRoutesListFragment.setCurrentType(RoutesListFragment.Type.Linear);
            view.findViewById(R.id.routes_map_fragment).setVisibility(View.GONE);
        }else if(!mIsTablet && !mIsPortrait){
            mRoutesListFragment.setCurrentType(RoutesListFragment.Type.Grid);
            view.findViewById(R.id.routes_map_fragment).setVisibility(View.GONE);
        }
        return view;
    }


    @Override
    public void onRouteSelected(@NotNull Route aRoute) {
        mRoutesMapFragment.setCenter(aRoute.getGeoPoint());
    }
}
