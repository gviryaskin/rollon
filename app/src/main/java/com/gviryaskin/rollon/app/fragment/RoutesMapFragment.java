package com.gviryaskin.rollon.app.fragment;

import android.support.v4.app.Fragment;

import com.gviryaskin.rollon.app.util.Logger;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.osmdroid.util.GeoPoint;

/**
 * Created by gviryaskin on 23.09.14.
 */
public class RoutesMapFragment extends Fragment {
    private static final String TAG="RoutesMapFragment";
    private static final Logger logger=new Logger(TAG);

    public void setCenter(@Nullable GeoPoint aGeoPoint){
        if(aGeoPoint==null){
            logger.error("GeoPoint is null!");
            return;
        }
        //TODO get controller set GeoPoint
    }

}
