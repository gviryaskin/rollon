package com.gviryaskin.rollon.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.gviryaskin.rollon.app.util.BaseActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class HomeActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    private final static ArrayList<Module> modules;
    static{
        modules=new ArrayList<Module>();
        //TODO create module drawable icons
        modules.add(new Module(R.drawable.ic_sun,R.string.module_routes,RoutesActivity.class));
        modules.add(new Module(R.drawable.ic_sun,R.string.module_tracker,TrackerActivity.class));
        modules.add(new Module(R.drawable.ic_sun,R.string.module_lessons,LessonsActivity.class));

    }

    private static class Module{
        int mIconResId,mNameResId;
        Class mActivityClass;
        public Module(int aIconResId,int aNameResId,Class aActivityClass){
            mIconResId=aIconResId;
            mNameResId=aNameResId;
            mActivityClass=aActivityClass;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //TODO create drawable log
        Picasso.with(this).load(R.drawable.ic_sun).into(((ImageView) findViewById(R.id.home_logo)));
        //TODO set bg to gridView from drawable
        GridView gridView = ((GridView) findViewById(R.id.home_grid));
        HomeAdapter adapter=new HomeAdapter();
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Module module=modules.get(position);
        Intent intent = new Intent(this,module.mActivityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        this.startActivity(intent);
    }

    private class HomeAdapter extends BaseAdapter{
        private class ViewHolder{
            private ImageView mImageView;
            private TextView mTextView;
        }
        @Override
        public int getCount() {
            return modules.size();
        }

        @Override
        public Object getItem(int position) {
            return modules.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView==null){
                convertView=getLayoutInflater().inflate(R.layout.item_module,parent,false);
                holder=new ViewHolder();
                holder.mImageView= ((ImageView) convertView.findViewById(R.id.module_icon));
                holder.mTextView= ((TextView) convertView.findViewById(R.id.module_name));
                convertView.setTag(holder);
            }else holder= ((ViewHolder) convertView.getTag());

            Module module=modules.get(position);
            Picasso.with(convertView.getContext()).load(module.mIconResId).into(holder.mImageView);
            holder.mTextView.setText(module.mNameResId);
            return convertView;
        }
    }
}
