package com.gviryaskin.rollon.app.fragment;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gviryaskin.rollon.app.LessonsActivity;
import com.gviryaskin.rollon.app.R;
import com.gviryaskin.rollon.app.RoutesActivity;
import com.gviryaskin.rollon.app.TrackerActivity;
import com.gviryaskin.rollon.app.util.BaseActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

//TODO make navigation list drawables like routes list drawables
//TODO use sp to show action bar 1st time
public class NavigationDrawerFragment extends Fragment{

    private LayoutInflater mLayoutInflater;
    private DrawerLayout mDrawerLayout;
    private View mFragmentView;
    private ActionBarDrawerToggle mDrawerToggle;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mLayoutInflater=inflater;
        ListView listView = ((ListView) inflater.inflate(R.layout.fragment_navigation_drawer,container,false));
        NavigationAdapter adapter=new NavigationAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(adapter);
        setHasOptionsMenu(true);
        return listView;

    }

    public void setUp(int fragmentId,@NotNull DrawerLayout drawerLayout) {
        mDrawerLayout=drawerLayout;
        mFragmentView=getActivity().findViewById(fragmentId);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        android.support.v7.app.ActionBar actionBar= ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        );

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private class NavigationAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
        private ArrayList<Item> mItems=new ArrayList<Item>();
        private abstract class Item{
            int mIconResId,mNameResId;
            public abstract void onClicked(@NotNull BaseActivity aBaseActivity);
            Item(int aIconResId,int aNameResId){
                mIconResId=aIconResId;
                mNameResId=aNameResId;
            }
        }

        NavigationAdapter(){
            mItems.clear();
            //TODO remove modules based on module
            //TODO create drawables
            mItems.add(new Item(R.drawable.ic_sun,R.string.nav_back) {
                @Override
                public void onClicked(@NotNull BaseActivity aBaseActivity) {
                    aBaseActivity.finish();
                }
            });
            mItems.add(new Item(R.drawable.ic_sun,R.string.module_routes) {
                @Override
                public void onClicked(@NotNull BaseActivity aBaseActivity) {
                    if(((Object) aBaseActivity).getClass()==RoutesActivity.class)return;
                    Intent intent=new Intent(aBaseActivity, RoutesActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    aBaseActivity.startActivity(intent);
                }
            });
            mItems.add(new Item(R.drawable.ic_sun,R.string.module_tracker) {
                @Override
                public void onClicked(@NotNull BaseActivity aBaseActivity) {
                    if(((Object) aBaseActivity).getClass()==TrackerActivity.class)return;
                    Intent intent=new Intent(aBaseActivity, TrackerActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    aBaseActivity.startActivity(intent);
                }
            });

            mItems.add(new Item(R.drawable.ic_sun,R.string.module_lessons) {
                @Override
                public void onClicked(@NotNull BaseActivity aBaseActivity) {
                    if(((Object) aBaseActivity).getClass()==LessonsActivity.class)return;
                    Intent intent=new Intent(aBaseActivity, LessonsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    aBaseActivity.startActivity(intent);
                }
            });

            mItems.add(new Item(R.drawable.ic_sun,R.string.nav_settings) {
                @Override
                public void onClicked(@NotNull BaseActivity aBaseActivity) {
                    aBaseActivity.showSettingsDialog();
                }
            });

            mItems.add(new Item(R.drawable.ic_sun,R.string.nav_about) {
                @Override
                public void onClicked(@NotNull BaseActivity aBaseActivity) {
                    aBaseActivity.showAboutDialog();
                }
            });

            mItems.add(new Item(R.drawable.ic_sun,R.string.nav_exit) {
                @Override
                public void onClicked(@NotNull BaseActivity aBaseActivity) {
                    aBaseActivity.finishApp();
                }
            });

        }
        private class ViewHolder {
            private ImageView mImageView1, mImageView2;
            private TextView mTextView;
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = getActivity().getLayoutInflater().inflate(R.layout.item_navigation, parent, false);
                holder.mImageView1 = ((ImageView) convertView.findViewById(R.id.navigation_icon1));
                holder.mImageView2 = ((ImageView) convertView.findViewById(R.id.navigation_icon2));
                holder.mTextView = ((TextView) convertView.findViewById(R.id.navigation_text));
                convertView.setTag(holder);
            } else holder = ((ViewHolder) convertView.getTag());

            //TODO make drawables
            Item item = mItems.get(position);
            if (position % 2 == 0) {
                Picasso.with(convertView.getContext()).load(item.mIconResId).into(holder.mImageView1);
                Picasso.with(convertView.getContext()).load(R.drawable.ic_sun).into(holder.mImageView2);
            } else {
                Picasso.with(convertView.getContext()).load(item.mIconResId).into(holder.mImageView2);
                Picasso.with(convertView.getContext()).load(R.drawable.ic_sun).into(holder.mImageView1);
            }
            holder.mTextView.setText(item.mNameResId);
            return convertView;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(getActivity()!=null)
                mItems.get(position).onClicked(((BaseActivity) getActivity()));
        }
    }
}
