package com.gviryaskin.rollon.app.dialog;

import com.gviryaskin.rollon.app.R;

import java.util.ArrayList;

/**
 * Created by gviryaskin on 08.05.14.
 */
public class DeleteRouteAppDialog extends AppDialogFactory.AppDialog{
    private static final ArrayList<Integer> BUTTONS_NAME_ID= new ArrayList<Integer>();
    static {
        BUTTONS_NAME_ID.add(android.R.string.ok);
        BUTTONS_NAME_ID.add(android.R.string.cancel);
    }

    public DeleteRouteAppDialog(AppDialogFactory.AppDialog.Callback callback) {
        super(callback);
    }

    @Override
    int getTitleId() {
        return R.string.dialog_route_delete_title;
    }

    @Override
    boolean isDialogCancelable() {
        return true;
    }

    @Override
    int getMessageId() {
        return R.string.dialog_route_delete_message;
    }

    @Override
    AppDialogFactory.AppDialog.ButtonsAmount getButtonsAmount() {
        return AppDialogFactory.AppDialog.ButtonsAmount.Two;
    }

    @Override
    ArrayList<Integer> getButtonsNameId() {
        return BUTTONS_NAME_ID;
    }
}
