package com.gviryaskin.rollon.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gviryaskin.rollon.app.userdata.UserDataManagement;
import com.gviryaskin.rollon.app.util.ActionBarTab;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by gviryaskin on 06.05.14.
 */
public class RouteFragment extends ActionBarTab {
    //Receiver
    public final static String ACTION_ROUTE_SWAP="SWAP_ROUTES";
    public final static String TAG_FRAGMENT_TYPE="TYPE";
    public final static String TAG_FRAGMENT_UPDATE="UPDATE";
    public final static String TAG_FRAGMENT_ID="ID";
    public final static String TAG_FRAGMENT_NEW="NEW";
    public enum FragmentType{List,Map}

    BroadcastReceiver mReceiver;

    private static Logger mLogger = LoggerFactory.getLogger(RouteFragment.class);
    //child fragments
    private FragmentType mCurrentChild=FragmentType.List;
    private RouteListFragment mListFragment;
    private RouteMapFragment mMapFragment;

    @Override
    public int getTabTitleId() {
        return R.string.title_section1;
    }

    @Override
    public void onBackPressed() {
        if(mCurrentChild.equals(FragmentType.List)){
            //Assume RouteFragmentList as root fragment and if user press back button, we finish activity
            super.onBackPressed();
        }else if(mCurrentChild.equals(FragmentType.Map)){
            //Else return to roots :)
            Intent intent = new Intent(ACTION_ROUTE_SWAP)
                    .putExtra(TAG_FRAGMENT_TYPE,FragmentType.List.ordinal());
            if(mMapFragment.needUpdateData())
                intent.putExtra(TAG_FRAGMENT_UPDATE,true);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route, container, false);

        mLogger.debug("Initiating child fragments");
        mListFragment= new RouteListFragment();
        mMapFragment=new RouteMapFragment();

        mReceiver= new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int type=intent.getIntExtra(TAG_FRAGMENT_TYPE,-1);
                if(type==-1)
                    throw new IllegalStateException("No fragment type received!");
                mLogger.debug("Replacing fragment");
                mCurrentChild=FragmentType.values()[type];
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                switch(FragmentType.values()[type]){
                    case List:
                        if(intent.getBooleanExtra(TAG_FRAGMENT_UPDATE,false))
                            mListFragment.updateDataList();
                        fragmentTransaction.replace(R.id.routeFragmentContainer,mListFragment);
                        break;
                    case Map:
                        if(!intent.getBooleanExtra(TAG_FRAGMENT_NEW, false)) {
                            long id= intent.getLongExtra(TAG_FRAGMENT_ID,-1);
                            if(id==-1)
                                throw new IllegalStateException("Fragment id is empty!");
                            mMapFragment.setRoute(UserDataManagement.getRoute(id));
                        }else mMapFragment.newRoute(getActivity());
                        fragmentTransaction.replace(R.id.routeFragmentContainer,mMapFragment);
                        break;
                }
                fragmentTransaction.commit();
            }

        };
        mLogger.debug("Initiating with roads list");
        getChildFragmentManager().beginTransaction().replace(R.id.routeFragmentContainer,mListFragment).commit();
        //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.routeFragmentContainer,mListFragment).commit();
        return view;
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver,new IntentFilter(ACTION_ROUTE_SWAP));
        super.onResume();
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
        super.onPause();
    }

}
