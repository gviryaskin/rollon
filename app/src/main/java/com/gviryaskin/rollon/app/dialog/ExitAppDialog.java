package com.gviryaskin.rollon.app.dialog;

import com.gviryaskin.rollon.app.R;

import java.util.ArrayList;

/**
 * Created by gviryaskin on 07.05.14.
 */
public class ExitAppDialog extends AppDialogFactory.AppDialog {
    private static final ArrayList<Integer> BUTTONS_NAME_ID= new ArrayList<Integer>();
        static {
            BUTTONS_NAME_ID.add(android.R.string.ok);
            BUTTONS_NAME_ID.add(android.R.string.cancel);
        }

    public ExitAppDialog(Callback callback) {
        super(callback);
    }

    @Override
    int getTitleId() {
        return R.string.dialog_exit_title;
    }

    @Override
    boolean isDialogCancelable() {
        return true;
    }

    @Override
    int getMessageId() {
        return R.string.dialog_exit_message;
    }

    @Override
    ButtonsAmount getButtonsAmount() {
        return ButtonsAmount.Two;
    }

    @Override
    ArrayList<Integer> getButtonsNameId() {
        return BUTTONS_NAME_ID;
    }
}
