package com.gviryaskin.rollon.app.util;

import android.support.v4.app.Fragment;

import com.gviryaskin.rollon.app.dialog.AppDialogFactory;

/**
 * Created by gviryaskin on 05.05.14.
 */
public abstract class ActionBarTab extends Fragment {
    /**
     * Represent tab Title in action bar
     * @return id of string in resource file
     */
    abstract public int getTabTitleId();

    /**
     * Called then user press back button on the current tab
     */
    public void onBackPressed(){
        AppDialogFactory.getDialog(AppDialogFactory.DialogId.Exit, new AppDialogFactory.AppDialog.Callback() {
            @Override
            public void onClick(int which) {
                if (which == 0)
                    getActivity().finish();
            }

            @Override
            public void onDismiss() {
            }

            @Override
            public void onCancel() {
            }
        }).show(getChildFragmentManager(),"Exit");
    }

}
