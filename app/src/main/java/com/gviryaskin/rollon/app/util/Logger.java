package com.gviryaskin.rollon.app.util;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {
    protected static boolean logEnabled=true;
    protected static boolean logToFile=false;

    protected static FileWriter fileWriter;

    protected synchronized void logToFile(@NotNull String aTag,@NotNull String aMessageType,@NotNull String aMessage){
        try {
            fileWriter.append(String.format("%s@%s@%s", aTag, aMessageType, aMessage));
        }catch (IOException eLog){
            throw new IllegalStateException("Error via log to file: ",eLog);
        }
    }

    public static void setLogEnabled(boolean aCondition){logEnabled=aCondition;}
    public static void setLogToFile(boolean aCondition,@Nullable File aFile){
        logToFile=aCondition;
        try {
            if (fileWriter != null) {

                fileWriter.flush();
                fileWriter.close();
                fileWriter = null;
            }
            fileWriter = new FileWriter(aFile, true);
        }catch (IOException e){
            throw new IllegalStateException("Error via creating/dispose FileWriter. Details: ",e);
        }
    }


    protected String mTag;
    protected org.slf4j.Logger mLogger;

    public Logger(@NotNull String aTag){
        mTag=aTag;
        mLogger= LoggerFactory.getLogger(mTag);
    }

    public void info(@NotNull String aMessage){
        if(!logEnabled)return;
        if(logToFile){
            logToFile(mTag,"INFO",aMessage);
            return;
        }else mLogger.info(aMessage);
    }

        public void debug(@NotNull String aMessage){
        if(!logEnabled)return;
        if(logToFile){
            logToFile(mTag,"DEBUG",aMessage);
            return;
        }else mLogger.debug(aMessage);
    }

        public void warn(@NotNull String aMessage){
        if(!logEnabled)return;
        if(logToFile){
            logToFile(mTag,"WARN",aMessage);
            return;
        }else mLogger.warn(aMessage);
    }

        public void error(@NotNull String aMessage){
        if(!logEnabled)return;
        if(logToFile){
            logToFile(mTag,"ERROR",aMessage);
            return;
        }else mLogger.error(aMessage);
    }

        public void error(@NotNull String aMessage,@NotNull Throwable aThrowable){
        if(!logEnabled)return;
        aMessage.contains(aThrowable.getLocalizedMessage());
        if(logToFile){
            logToFile(mTag,"INFO",aMessage);
            return;
        }else mLogger.info(aMessage);
    }


}
