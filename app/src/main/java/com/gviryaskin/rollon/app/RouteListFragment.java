package com.gviryaskin.rollon.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gviryaskin.rollon.app.userdata.UserDataManagement;
import com.gviryaskin.rollon.app.userdata.UserRoute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;

public class RouteListFragment extends Fragment {


    //List
    private static Logger mLogger = LoggerFactory.getLogger(RouteFragment.class);
    private ListView mListView;
    private RouteListAdapter mAdapter;

    public RouteListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_list, container, false);

        // Set the adapter
        mAdapter=new RouteListAdapter(inflater);
        mAdapter.setData(UserDataManagement.getUserRoutes(getActivity()));

        mListView = (ListView) view.findViewById(android.R.id.list);
        View header = inflater.inflate(R.layout.fragment_route_header,mListView,false);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLogger.debug("New route button clicked");
                Intent intent = new Intent(RouteFragment.ACTION_ROUTE_SWAP)
                        .putExtra(RouteFragment.TAG_FRAGMENT_TYPE,RouteFragment.FragmentType.Map.ordinal())
                        .putExtra(RouteFragment.TAG_FRAGMENT_NEW,true);
                LocalBroadcastManager.getInstance(v.getContext()).sendBroadcast(intent);
            }
        });
        View footer = inflater.inflate(R.layout.fragment_route_footer,mListView,false);
        mListView.addHeaderView(header);
        mListView.addFooterView(footer);
        mListView.setAdapter(mAdapter);
        return view;
    }
    public synchronized void updateDataList(){
        mLogger.debug("Updating list");
        mAdapter.setData(UserDataManagement.getUserRoutes(getActivity()));
        mListView.invalidate();
    }
    /////////////////////////////////////////////
    //adapter
    private static class RouteListAdapter extends BaseAdapter{
        private Logger mLogger= LoggerFactory.getLogger(RouteListAdapter.class);
        private ArrayList<UserRoute> mRoutes=new ArrayList<UserRoute>();
        private LayoutInflater mLayoutInflater;
        RouteListAdapter(LayoutInflater inflater){mLayoutInflater=inflater;}
        public void setData(ArrayList<UserRoute> routes){
            mRoutes.clear();
            if(routes!=null)
                mRoutes.addAll(routes);
            Collections.sort(mRoutes);
        }
        @Override
        public int getCount() {
            return mRoutes.size();
        }

        @Override
        public Object getItem(int position) {
            return mRoutes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mRoutes.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null)
                convertView=mLayoutInflater.inflate(R.layout.fragment_route_item,parent,false);
            UserRoute route = (UserRoute)getItem(position);
            if(route==null){
                mLogger.error("Something goes wrong with item at position: "+position);
                return null;
            }
            convertView.setTag(route.getId());
            //swaping route image with item picture
            ImageView imgStart=(ImageView)convertView.findViewById(R.id.routeImageStart);
            ImageView imgEnd=(ImageView)convertView.findViewById(R.id.routeImageEnd);

            if(position%2!=0){
                imgStart.setImageResource(route.getIcon());
                imgEnd.setImageResource(R.drawable.ic_launcher); //TODO implement placeholder
            }else{
                imgStart.setImageResource(R.drawable.ic_launcher);
                imgEnd.setImageResource(route.getIcon());
            }
            ((TextView)convertView.findViewById(R.id.routeName)).setText(route.getName());
            //TODO think about route length
            convertView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    long id= ((Long) v.getTag());
                    mLogger.debug("Selected route with id: %d"+id);
                    Intent intent = new Intent(RouteFragment.ACTION_ROUTE_SWAP)
                        .putExtra(RouteFragment.TAG_FRAGMENT_TYPE,RouteFragment.FragmentType.Map.ordinal())
                        .putExtra(RouteFragment.TAG_FRAGMENT_ID,id);
                    LocalBroadcastManager.getInstance(v.getContext()).sendBroadcast(intent);

                }
            });

            return convertView;
        }
    }

}
