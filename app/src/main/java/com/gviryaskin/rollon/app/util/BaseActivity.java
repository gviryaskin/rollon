package com.gviryaskin.rollon.app.util;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;

import com.gviryaskin.rollon.app.HomeActivity;
import com.gviryaskin.rollon.app.R;


public abstract class BaseActivity extends ActionBarActivity {
    private static boolean needToFinishApp=false;
    public void finishApp(){
        needToFinishApp=true;
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(needToFinishApp) {
            finish();
            if(((Object) this).getClass()== HomeActivity.class)
                needToFinishApp=false;
        }
    }

    public void showSettingsDialog() {
        //TODO create custom dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.nav_settings)
                .setMessage(R.string.placeholder)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    public void showAboutDialog() {
        //TODO create custom dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.nav_about)
                .setMessage(R.string.placeholder)
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }
}
