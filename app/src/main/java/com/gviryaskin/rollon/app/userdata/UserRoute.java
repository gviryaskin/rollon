package com.gviryaskin.rollon.app.userdata;

import com.gviryaskin.rollon.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import java.util.LinkedList;

/**
 * Created by gviryaskin on 05.05.14.
 */
public class UserRoute implements Comparable<UserRoute>{

    @Override
    public int compareTo(UserRoute another) {
        return mName.compareTo(another.getName());
    }

    public enum Icon{Skate,Smile,Star,Skull,Park,City,Night,Sun}
    //Base
    long mId;
    String mName;
    Icon mIcon=Icon.Skate;
    //Detail
    boolean mIsLoaded=false;
    String mDescription;
    int mPointAmount;
    long mLength;
    LinkedList<Point> mPoints;

    //getters&setters
    public long getId(){return mId;}

    public String getName(){return mName;}
    public void setName(String name){mName=name;}

    public int getIcon(){
        return  getIconId(mIcon);
    }
    public void setIcon(Icon icon){mIcon=icon;}

    public boolean isLoaded(){return mIsLoaded;}

    public String getDescription(){return mDescription;}
    public void setDescription(String description){mDescription=description;}

    public int getPointAmount(){return mPointAmount;}
    public long getLength(){return mLength;}
    //Constructors
    public UserRoute(){
        mId= ((Object) this).hashCode();
        mIcon=Icon.Sun;
        mIsLoaded=true;
        mPointAmount=0;
        mLength=0;
        mPoints=new LinkedList<Point>();
    }

    public UserRoute(UserRoute route){
        mId=route.mId;
        mName=route.mName;
        mIcon=route.mIcon;
        mDescription=route.mDescription;
        mIsLoaded=route.mIsLoaded;
        mPointAmount=route.mPointAmount;
        mLength=route.mLength;
        mPoints=new LinkedList<Point>();
        for(Point point : route.mPoints)
            mPoints.add(new Point(point));

    }
    /////////////////////////////////////////////
    //JSON
    //list
    private static final String TAG_ID="id";
    private static final String TAG_NAME="name";
    private static final String TAG_ICON="icon";
    //detail
    private static final String TAG_DESCRIPTION="description";
    private static final String TAG_POINT_AMOUNT="pointAmount";
    private static final String TAG_LENGTH="length";
    private static final String TAG_POINTS="points";

    public boolean setJSONData(JSONObject jObj){
        try{
            mId=jObj.getInt(TAG_ID);
            mName=jObj.getString(TAG_NAME);
            mIcon=Icon.values()[jObj.getInt(TAG_ICON)];
            return true;
        }catch(JSONException jException){return false;}

    }
    public JSONObject getJSONData(){
        try {
            JSONObject jObj = new JSONObject();
            jObj.put(TAG_ID, mId).put(TAG_NAME, mName).put(TAG_ICON,mIcon.ordinal());
            return jObj;
        }catch (Exception e){return null;}
    }
    public boolean setJSONDetail(JSONObject jObj){
        try{
            mIsLoaded=true;
            mDescription=jObj.getString(TAG_DESCRIPTION);
            mPointAmount=jObj.getInt(TAG_POINT_AMOUNT);
            mLength=jObj.getLong(TAG_LENGTH);
            JSONArray jPoints = jObj.getJSONArray(TAG_POINTS);
            mPoints.clear();
            for(int i=0;i<jPoints.length();i++) {
                Point point = new Point();
                if(point.setJSONData(jPoints.getJSONObject(i)))
                    mPoints.add(point);
            }
            return true;
        }catch(JSONException jException){return false;}
    }
    public JSONObject getJSONDetail(){
        try {
            JSONObject jObj = new JSONObject();
            jObj.put(TAG_DESCRIPTION, mDescription);
            jObj.put(TAG_POINT_AMOUNT,mPointAmount);
            jObj.put(TAG_LENGTH,mLength);
            JSONArray jPoints = new JSONArray();
            for(Point point: mPoints){
                JSONObject jPoint =point.getJSONData();
                if(jPoint!=null)
                    jPoints.put(jPoint);
            }
            jObj.put(TAG_POINTS,jPoints);
            return  jObj;
        }catch (JSONException jException){
            return null;
        }
    }
    public static class Point{

        String mName;
        String mDescription;
        GeoPoint mGeoPoint;

        public void setName(String name){mName=name;}
        public void setDescription(String description){mDescription=description;}
        public void setGetPoint(GeoPoint geoPoint){mGeoPoint=geoPoint;}

        public String getName(){return mName;}
        public String getDescription(){return  mDescription;}
        public GeoPoint getGeoPoint(){return mGeoPoint;}

        public Point() {}
        public Point(Point point){
            mName=point.mName;
            mDescription=point.mDescription;
            mGeoPoint= point.mGeoPoint.clone(); //TODO Check
        }
        /////////////////////////////////////////
        //JSON
        private final static String TAG_LATITUDE="latitude";
        private final static String TAG_LONGITUDE ="longitude";
        public boolean setJSONData(JSONObject jObj){
            try{
                mName=jObj.getString(TAG_NAME);
                mDescription=jObj.getString(TAG_DESCRIPTION);
                mGeoPoint=new GeoPoint(jObj.getDouble(TAG_LATITUDE),jObj.getDouble(TAG_LONGITUDE));
                return true;
            }catch(JSONException jException){return false;}
        }
        public JSONObject getJSONData(){
            try{
                JSONObject jObj= new JSONObject();
                jObj.put(TAG_NAME,mName);
                jObj.put(TAG_DESCRIPTION,mDescription);
                jObj.put(TAG_LATITUDE,mGeoPoint.getLatitude());
                jObj.put(TAG_LONGITUDE,mGeoPoint.getLongitude());
                return jObj;
            }catch (JSONException jException){return null;}
        }
    }

    /////////////////////////////////////////////
    //Static
    public static int getIconId(Icon icon){
        //TODO make image res files
        switch (icon){
            case Skate:
                break;
            case Smile:
                break;
            case Star:
                break;
            case Skull:
                break;
            case Park:
                break;
            case City:
                break;
            case Night:
                break;
            case Sun:
                return R.drawable.ic_sun;
        }
        return R.drawable.ic_sun;
    }
}
